//
//  main.cpp
//  OS LR-2
//
//  Created by Дмитрий Пархоменко on 03.03.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#pragma intrinsic(__rdtsc)using namespace std;
using namespace std;

unsigned long long rdtsc() {    //возвращает текущий такт процессора
    return __rdtsc();
}

void OutMassToConsole(int *size, int *mass) {   //вывод массива в консоль
    for (int j=0; j<*size; j++) cout<< mass[j] << "\t";
    cout<< endl;
}

void Fifo(int sizeOzu, int sizeVirt, int *massOzu, int *massVirt) {
    int *Regedit = new int[sizeOzu]; //список используемых элементов
    for (int i=0; i<sizeOzu; i++) Regedit[i]=-1;
    
    cout << "\rOZU:\r";
    for (int i=0; i<sizeOzu; i++) cout <<"i" << i+1 << "\t";
    cout <<endl;
    
    unsigned long long startTact = rdtsc();
    int replasments = 0;
    
    for (int i=0; i<sizeVirt; i++) {//проход по массиву вирт. памяти и замещение в оперативную
        int positionElementInRegedit=-1;
        int firstEmptyPosition=-1;
        for (int j=0; j<sizeOzu; j++) if (Regedit[j]==massVirt[i]) positionElementInRegedit = j;
        for (int j=0; j<sizeOzu; j++) if (massOzu[j]==0) {firstEmptyPosition=j; break;}

        if (positionElementInRegedit==-1) {     //нового элемента нет в Regedit
            if (firstEmptyPosition!=-1) {       //есть пустое место
                massOzu[firstEmptyPosition] = massVirt[i];
                Regedit[firstEmptyPosition] = massVirt[i];
            } else {    //нет места для нового элемента
                for (int j=0; j<sizeOzu; j++) { //проход по массиву ОЗУ, что бы найти где удалять элемент
                    if (massOzu[j]==Regedit[0]) {
                        massOzu[j] = massVirt[i];
                        for (int k=0; k<sizeOzu; k++) { //перемещение элементов в Regedit
                            if(k< sizeOzu-1)Regedit[k] = Regedit[k+1];
                            else Regedit[sizeOzu-1] = massVirt[i];
                        }
                        break;
                    }
                }
                replasments++;
            }
        }
        
        OutMassToConsole(&sizeOzu, massOzu);    //вывод массива ОЗУ после каждого шага
    }
    cout << "Количество тактов: " << rdtsc()-startTact << endl << "Количество перестановок в ОЗУ: " <<  replasments << endl;
    //cout << "LongesTimeElementsNotUsableInOzu:\t";
    //OutMassToConsole(&sizeOzu, Regedit);
    delete [] Regedit;
}

void Opt(int sizeOzu, int sizeVirt, int *massOzu, int *massVirt) {
    cout << "\rOZU:\r";
    for (int i=0; i<sizeOzu; i++) cout <<"i" << i+1 << "\t";
    cout<<endl;
    
    unsigned long long startTact = rdtsc();
    int replasments = 0;
    
    for (int i=0; i<sizeVirt; i++) {    //проход по массиву вирт. памяти и замещение в оперативную
        bool elementInOzu = false;      //есть ли уже эта страница в озу
        int firstEmptyPosition=-1;      //положение первой пустой ячейки
        int max = 0;
        int indexMax;
        for (int j=0; j<sizeOzu; j++) if (massOzu[j]==massVirt[i]) {elementInOzu = true; break;}
        for (int j=0; j<sizeOzu; j++) if (massOzu[j]==0) {firstEmptyPosition=j; break;}
        
        if (!elementInOzu) {                //если страницы нет в озу
            if (firstEmptyPosition!=-1) {   //есть пустое место
                massOzu[firstEmptyPosition] = massVirt[i];
            } else {                        //нет пустого места
                int* Regedit = new int[sizeOzu];    //количество проходов через которое элемент будет задействован
                for (int j=0; j<sizeOzu; j++) Regedit[j] = sizeVirt+1;
                
                for (int j=0; j<sizeOzu; j++) { //проход по всем элементам в ОЗУ
                    for (int k=i; k<sizeVirt; k++) {    //создание массива со значениями неиспользования элемнтов
                        if (massOzu[j]==massVirt[k]) {Regedit[j]=sizeVirt-i; break;}
                    }
                }
                
                for (int j=0; j<sizeOzu; j++) { //нахождение максимально неиспользуемой страницы и ее индекса
                    if (Regedit[j]>max) {
                        max = Regedit[j];
                        indexMax = j;
                    }
                }
                
                massOzu[indexMax]=massVirt[i];
                replasments++;
                delete [] Regedit;
            }
        }
        
        OutMassToConsole(&sizeOzu, massOzu);    //вывод массива ОЗУ после каждого шага
    }
    cout << "Количество тактов: " << rdtsc()-startTact << endl << "Количество перестановок в ОЗУ: " <<  replasments << endl;
}


int main() {
    
    int n,sizeVirt,sizeOzu,maxVal;
    
    do {
        cout << "0.Выход\r1.Создать массив, провести замену страниц по методу FIFO\r2.Создать массив, провести замену страниц по методу OPT\r";
        cin >> n;
        if (n==0) break;
        
        cout << "Введите размерность массива Виртуальной памяти: ";
        cin >> sizeVirt;
        cout << "Введите размерность ОЗУ: ";
        cin >> sizeOzu;
        cout << "Введите максимальное значение числа: ";
        cin >> maxVal;
        
        int *massVirt = new int[sizeVirt];  //массив Виртуальной памяти
        int *massOzu = new int[sizeOzu];    //массив ОЗУ
        for (int i=0; i<sizeOzu; i++) massOzu[i] = 0;   //обнуление массива ОЗУ
        
        //for (int i=0; i<sizeVirt; i++) cin >> massVirt[i];// ввод массива вручную
        for (int i=0; i<sizeVirt; i++) massVirt[i] = 1+rand()%maxVal;   //заполнение массива вирт. памяти
        
        cout << "\rRandARR:";
        OutMassToConsole(&sizeVirt, massVirt);
        
        if (n==1) Fifo(sizeOzu, sizeVirt, massOzu, massVirt);
        if (n==2) Opt(sizeOzu, sizeVirt, massOzu, massVirt);
        
        delete [] massVirt, massOzu;
    } while (n!=0);
    
    return 0;
}
