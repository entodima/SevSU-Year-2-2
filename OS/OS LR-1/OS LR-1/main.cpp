//
//  main.cpp
//  OS LR-1
//
//
//
//

#include <iostream>
#include <ctime>
using namespace std;


int main() {
    int n;
    do {
        cout << "1.Задание1"<<endl <<"2.Задание2" << endl << "0.Выход" << endl;
        cin >> n;
        unsigned int start_time = clock();
        if (n == 1) {
            //Задание 1
            cout << "N1" << endl;
            int P=1;
            
            int size;
            cout << "Размер вектора: ";
            cin >> size;
            
            int *massOfStrings = new int[size];
            
            //заполнение массива
            for (int i = 0; i<size; i++) {
                cout << "Элемент  [" << i  << "]: ";
                cin >> massOfStrings[i];
            }
            
            for(int i = 0; i<size; i++)
            {
                if (abs(massOfStrings[i]%2) == 1 ) {
                    cout << i << ", ";
                    P = P * massOfStrings[i];
                    cout << P << endl;
                }
            }
            cout << "P = " << P << endl;
            
        }
        
        if (n==2){
            //Задание 2
            cout << endl << "N2" << endl;
            
            int size;
            cout << "Размер массива: ";
            cin >> size;
            
            int *massOfStrings = new int[size];
            
            //создание динамического массива
            float **ptr_mass = new float*[size];
            for (int i = 0; i < size; i++) {
                ptr_mass[i] = new float[size];
                massOfStrings[i] = 0;   //обнуление массива результата
            }
            
            //заполнение массива
            for (int i = 0; i<size; i++) {
                cout << "строка " << i << endl;
                for (int j = 0; j<size; j++) {
                    cout << "элемент " << j << ": ";
                    cin >> ptr_mass[i][j];
                }
            }
            
            //определение количества ненулевых объектов
            for (int i =0; i<size; i++) for (int j=0; j<size; j++) if (ptr_mass[i][j]!=0) massOfStrings[i]++;
            
            //вывод массивов
            for (int i = 0; i<size; i++) {
                for (int j = 0; j<size; j++) cout<<ptr_mass[i][j];
                cout << "\t" << massOfStrings[i] << endl;
            }
            cout << "time: " << clock() - start_time<< endl;
            
            //удаление динамических переменных
            for (int i =0; i<size; i++) delete[] ptr_mass[i];
            delete[] massOfStrings;
        }
    } while(n != 0);
    return 0;
}
