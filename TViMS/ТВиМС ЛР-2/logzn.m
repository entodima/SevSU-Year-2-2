function [ result ] = logzn( am, aM, x )
    if x >= am && x < aM
        result = 1;
    else result = 0;
end

