//
//  main.cpp
//  OTA LR-1
//
//  Created by Дмитрий Пархоменко on 15.02.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

int main() {
    double Fn,  //fn
     Gn,    //gn
     ATg_FG,
     ATg_GF,
     r, //разность между FG и GF
     lBorder,   //Левая граница интервала
     rBorder,   //Правая граница интервала
     step,  //шаг изменения аргумента
     phi,   //значение угла;
     k, //коофицент кратности
     Delta, //Оценка Дельта
     Theta, //Оценка тетта
     O_large,   //Оценка О-большое
     n; //значение аргумента функций трудоемкости
    
    cout << "Левая граница интервала: ";
    cin >> lBorder;
    cout << "Правая граница интервала: ";
    cin >> rBorder;
    cout << "Шаг изменения аргумента: ";
    cin >> step;
    cout << "Коэфицент кратности: ";
    cin >> k;
    
    phi = M_PI / k;
    n = lBorder;
    
    ofstream out("result.txt", ios_base::app);
    out << "n:\tF(n):\t\tG(n):\t\tDelta:\t\tTheta:\t\tO:"<<endl;
    while (n<=rBorder) {
        Fn = 8*pow(n,3) + 3*n*log(n) + 18*n;    //расчет значения функции Fn
        Gn = 48*pow(n, log2(7));
        ATg_FG = atan(Fn/Gn);
        ATg_GF = atan(Gn/Fn);
        
        r = ATg_FG - ATg_GF;    //разность арктангенсов
        Delta = phi - r;    //вычисление оценки дельта
        Theta = abs(r) - phi;
        O_large = r+phi;    //вычисление Обольшое
        
        out << n << "\t" << Fn << "\t\t" << Gn << "\t\t" << Delta << "\t\t" << Theta << "\t" << O_large << endl;
        n += step;
    }
    out.close();
    
    return 0;
}
