//
//  main.cpp
//  OTA LR-6
//
//  Created by Дмитрий Пархоменко on 18.04.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//
//  Модифицированный метод «пузырька»
//  Сортировка посредством выбора
//  «Быстрая» сортировка
//
//  Структура дата, содержит день, месяц и год, сортировка по убыванию дат
//

#include <iostream>
using namespace std;
#include <cstdlib>
#pragma intrinsic(__rdtsc)using namespace std;

unsigned long long rdtsc() {    //возвращает текущий такт процессора
    return __rdtsc();
}


void bubbleSort(int *array, int size) {
    int temp;
    for(int i=0;i<size;i++) {            // i - номер прохода
        for(int j=size-1;j>i;j--) {     // внутренний цикл прохода
            if(array[j-1]<array[j]) {
                temp=array[j-1];
                array[j-1]=array[j];
                array[j]=temp;
            }
        }
    }
}

void selectSort(int *array, int size) { // сортировка выбором
    for (int i=0; i<size; i++) {
        int temp = array[0]; // временная переменная для хранения значения перестановки
        for (int j = i + 1; j < size; j++) {
            if (array[i] > array[j]) {
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }
}
void quickSort(int *array, int size) {
    int i = 0, j = size; // поставить указатели на исходные места
    int temp, p;
    p = array[size/2]; // центральный элемент
    // процедураразделения
    do {
    while (array[i] < p) i++;
    while (array[j] > p) j--;
    if (i <= j) {
        swap(array[i], array[j]);
        i++; j--;
    } }
while(i <= j);
    //рекурсивные вызовы, если есть, что сортировать
    if(j > 0) quickSort(array, j);
    if(size > i) quickSort(array+i, size-i);
}

int main() {
    int size = 20;
    int vect[20] = {20181116, 20181117, 20161116, 19991116, 18031116, 19971116,20061116, 20171231, 20131009, 20131008, 20121212, 20121221, 20121224, 20150829, 20111119, 20150630, 20060606, 20070606, 20060605, 20060405};
    unsigned long long startTact, finishTact;
    
    startTact = rdtsc();
    quickSort(vect, size);
    finishTact = rdtsc() - startTact;
    
    for (int i=0; i<size; i++) cout << vect[i] << endl;
    cout << "Количество тактов: "<< finishTact << endl;
    
    return 0;
}
