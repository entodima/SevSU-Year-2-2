//
//  main.cpp
//  OTA LR-5
//
//  Created by Дмитрий Пархоменко on 05.04.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//
//  ПОИСК КРАТЧАЙШИХ ПУТЕЙ НА ГРАФАХ

#include <iostream>
#include <iomanip>
#include <algorithm>

#include <cstdlib>
#pragma intrinsic(__rdtsc)using namespace std;
using namespace std;

const int INF = 1000000000; //бесконечность
const int size = 9;         //количество вершин
bool used[size] = {0};      //массив для пометок
int top[size] = {0};        //топологический список
int g[size-1][size];  //матрица смежности
int l=0;    //l-я вершина добавления
int s=0;    //стартовая вершина
int f;      //конечная вершина
int d[size] = {0};          //массив ответов


unsigned long long rdtsc() {    //возвращает текущий такт процессора
    return __rdtsc();
}

//алгоритм Дейкстры
void Algorithm1(int GR[size-1][size],int st) {
    int distance[size], count, index = 0, i, u, m=st+1;
    bool visited[size];
    
    for (int i=0; i<size; i++) {
        distance[i] = INF;
        visited[i] = false;
    }
    distance[st] = 0;
    
    for (count=0; count<size-1; count++) {
        int min =INF;
        for (i=0; i<size; i++) {
            if (!visited[i] && distance[i] <= min) {
                min = distance[i];
                index=i;
            }
        }
        u=index;
        visited[u] =true;
        for (i=0; i<size; i++) {
            if (!visited[i] && GR[u][i] && distance[u]!=INF && distance[u]+GR[u][i]<distance[i]) {
                distance[i] = distance[u] + GR[u][i];
            }
        }
    }
    
    cout << "Путь: " << endl;
    for (i=0; i<size; i++) {
        if (distance[i] != INF) {
            cout << m << " > " << i+1 << " = " << distance[i] << endl;
        } else {
            cout << m << " > " << i+1 << " = " << " пути нет" << endl;
        }
    }
}

int dfs(int v) {
    if (used[v]) return 0;
    used[v] = true;
    for (int to=0; to<size; to++) if (g[v][to]) dfs(to);
    top[l++]=v; //добавление вершины в отсортированный список
    return NULL;
}


//алгоритм топологической сортировки вершин
int topSort(){
    l=0;    //номер добавляемой вершины в отсортированный список
    for (int i=0; i<size; i++) dfs(i); //проход по всем вершинам
    reverse(top, top+l);    //развернуть массив
    return 0;
}


//метод динамического программирования
int Algorithm2() {
    int i,j;
    for (i=0; i<size; i++) d[i] = INF;
    d[s]=0;
    for (i=0; i<size; i++) for (j=0; j<i; j++) if (g[top[j]][top[i]]) d[top[i]]=min(d[top[i]], d[top[j]]+g[top[j]][top[i]]);
    return 0;
}



int main() {
    int massIn[size-1][size] = {
        {0,2,1,0,0,0,0,0,0},
        {0,0,7,2,0,0,0,0,0},
        {0,0,0,0,2,3,0,6,0},
        {0,0,3,0,0,0,8,0,0},
        {0,0,0,0,0,0,4,0,0},
        {0,0,0,0,3,0,0,0,0},
        {0,0,0,0,0,2,0,0,10},
        {0,0,0,0,0,0,0,0,1}
    };
    for (int i=0; i<size-1; i++) for (int j=0; j<size; j++)  g[i][j] = massIn[i][j];
    
    cout << "Алгоритм Дейкстры" << endl;
    unsigned long long startTact = rdtsc();
    Algorithm1(g, s);
    unsigned long long finishTact = rdtsc() - startTact;
    cout << "Количество тактов: " << finishTact << endl;
    
    cout << endl << "Метод динамического программирования" << endl;
    startTact = rdtsc();
    topSort();
    Algorithm2();
    finishTact = rdtsc() - startTact;
    cout << "Путь: " << endl;
    for (int i=0; i<size; i++) cout << s+1 << " > " << i+1 << " = " << d[i] << endl;
    
    cout << "Количество тактов: " << finishTact << endl;
    
    return 0;
}
