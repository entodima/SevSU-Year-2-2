//
//  main.cpp
//  OTA LR-8
//
//  Created by Дмитрий Пархоменко on 02.05.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
using namespace std;

const int INF = 1000000000;
const int size = 9;
bool used[size] = {0};
int g[size][size] = {0};

void Solve(int g[size][size]) {
    int a = 0, b = 0, u, v = 0, k=1, mincost=0;
    int path[100] = {0};  //массив с вершинами, по которым состовляется путь
    int pathIndex = 0;
    
    for (int i=0; i<size; i++)
        for (int j=0; j<size; j++)
            if (g[i][j]==0) g[i][j] = INF;
    
    used[0]=1;
    
    while (k<size) {
        for (int i=0, min=INF; i<size; i++)
            for (int j=0; j<size; j++)
                if (g[i][j]<min) {
                    if (used[i] != 0) {
                        min = g[i][j];
                        a=u=i;
                        b=v=j;
                    }
                    if (used[i]==0 || used[v]==0) {
                        path[pathIndex] = b;
                        pathIndex++;
                        cout << "   " << a+1 << "====>" << b+1 << " длина = " << min << endl;
                        mincost += min;
                        used[b]=1;
                        k++;
                    }
                }
        g[a][b] = g[b][a] = INF;
    }
    cout << endl << "\n Длина = " << mincost << endl;
}

int main() {
    int mass[size][size] = {
        {0,2,1,0,0,0,0,0,0},
        {2,0,7,2,0,0,0,0,0},
        {1,7,0,3,2,3,0,6,0},
        {0,1,3,0,0,0,8,0,0},
        {0,0,2,0,0,3,4,0,0},
        {0,0,3,0,3,0,2,0,0},
        {0,0,0,8,0,2,0,0,10},
        {0,0,6,4,0,0,10,0,1}
    };
    
    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            g[i][j]=mass[i][j];
        }
    }
    
    cout << "Пути:"<< endl;
    Solve(g);
    
    return 0;
}
