//
//  main.cpp
//  OTA LR-2
//
//  Created by Дмитрий Пархоменко on 27.02.2018.
//  Copyright © 2018 Дмитрий Пархоменко. All rights reserved.
//

#include <iostream>
#include <fstream>
using namespace std;

//Заполенение массива случайными числами
void FillRandMass(int *size, int *max, int *mass) {
    for (int i = 0; i<*size; i++) {
        mass[i] = 1+rand()%*max;
    }
}

//Вывод массива в файл
void MassFileOut(int *size, int *mass) {
    ofstream fout("result.txt", ios_base::app);
    for (int i = 0; i<*size; i++) {
        fout << i+1<< "\t" << mass[i] << endl ;
    }
    cout << endl;
}

//Нахождение минимального значения в массиве и вывод в консоль значения n-гармонического числа
void FindMin(int *size, int *max, int *mass) {
    int min = *max;
    int n = 0;
    float summN=0;
    
    for (int i = 0; i<*size; i++) if (mass[i] < min) { min = mass[i]; n++; } //перестановка
    for (float i = 1; i<=n; i++) summN += (1.0/i); //подсчет n-гармонического числа
    summN /= n;
    
    cout << "Минимальное значение: "<<  min << "\rn-гармоническое число для массива: "<< summN << endl;
    cout << "n = " << n << endl;
}

int main() {
    int n;
    do {
        cout << "0.Выход\r1.Создать новый массив и вывести в файл\r2.Создать новый массив, подсчитать минимум(на экран) и вывести в файл\r3.Создать новый массив, подсчитать минимум и вывести на экран\r";
        cin >> n;
        
        int size, max;
        if (n==1 || n==2 || n==3) {
            cout << "Размерность массива: ";
            cin >> size;
            cout << "Максимальное значение: ";
            cin >> max;
        }
        
        int *mass = new int[size];
        
        if (n==1) {
            FillRandMass(&size, &max, mass);    //заполнение массива случайными числами
            MassFileOut(&size, mass);           //вывод массива в файл
        }
        if (n==2) {
            FillRandMass(&size, &max, mass);    //заполнение массива случайными числами
            FindMin(&size, &max, mass);         //нахождение минимального значения
            MassFileOut(&size, mass);           //вывод массива в файл
        }
        if (n==3) {
            FillRandMass(&size, &max, mass);    //заполнение массива случайными числами
            FindMin(&size, &max, mass);         //нахождение минимального значения
        }
        
        delete[] mass;
    } while (n!=0);
    return 0;
}
