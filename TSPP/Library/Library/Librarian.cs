﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library
{
    public class Librarian
    {
        public Librarian(){
        }

        private string name;
        private int id;

        public Library TakeBook(Library library, int bookId, Reader reader) {
            library.books[bookId - 1].Take(reader);
            return library;
        }

        public Library ReturnBook(Library library, int bookId, Reader reader) {
            library.books[bookId - 1].Return();
            return library;
        }

        public void GetTakenBooksListByReader(Library library, Reader reader) {
            library.SeeIsssuedBooksByReader(reader);
        }

        public void SeeAllCatalog(Library library) {
            library.SeeAllCatalog();
        }
        public void SeeTheCatalog(Library library) {
            library.SeeTheCatalog();
        }
        public void SeeIssuedCatalog(Library library) {
            library.SeeIssuedCatalog();
        }
    }
}
