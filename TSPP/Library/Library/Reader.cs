﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    public class Reader  {
        public Reader(int id) {
            this.id = id;
            name = "Johnny Depp";
            phone = 0800400388;
        }

        protected int id;
        public string name;
        protected long phone;

        public void GetTakenBooksList(Library library) {
            library.SeeIsssuedBooksByReader(this);
        }

        public void Show() {
            Console.WriteLine("ReaderId:" + id + " Name:" + name + " Phone:" + phone);
        }

        public void SeeTheCatalog(Library library){
            library.SeeTheCatalog();
        }
    }
}
