﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    public class PrivilegedReader : Reader {
        public PrivilegedReader(int id) : base(id) {
            this.id = id;
            name = "Nolan Christopher";
            phone = 33244923;
        }
        private int id;

        public Library TakeBook(Library library, int bookId, Reader reader) {
            library.books[bookId - 1].Take(reader);
            return library;
        }

        public Library ReturnBook(Library library, int bookId, Reader reader) {
            library.books[bookId - 1].Return();
            return library;
        }
    }
}
