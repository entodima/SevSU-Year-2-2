﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    public class Author {
        public Author() {
        }
        public Library Library { get; set; }

        private int id;
        private string name;
        private string description;

        private int bookCounter = 0;
        private List<Book> books = new List<Book>();
    }
}