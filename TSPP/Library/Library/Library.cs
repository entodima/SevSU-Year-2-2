﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    public class Library {
        public Library() {
            //authors = new List<Author>();
            books = new List<Book>();

            Console.WriteLine("---Библиотека создана");
        }

        public IList<Book> books { get; set; }
        //private IList<Author> authors { get; }


        //каталог невыданных книг
        public void SeeTheCatalog() {
            if (books.Count != 0) {
                Console.WriteLine("---Книги");
                foreach (Book book in books) {
                    book.Out();
                }
            } else Console.WriteLine("---Книг нет!");
        }

        //каталог выданных книг
        public void SeeIssuedCatalog() {
            if (books.Count != 0) {
                Console.WriteLine("---Книги");
                foreach (Book book in books) {
                    book.OutAll();
                }
            } else Console.WriteLine("---Книг нет!");
        }
        //каталог всех книг
        public void SeeAllCatalog() {
            if (books.Count != 0) {
                Console.WriteLine("---Книги");
                foreach (Book book in books) {
                    book.OutAll();
                }
            } else Console.WriteLine("---Книг нет!");
        }

        public void SeeIsssuedBooksByReader(Reader reader){
            if (books.Count != 0) {
                Console.WriteLine("---Книги, выданные читателю " + reader.name);
                foreach (Book book in books) {
                    book.OutIssued(reader);
                }
            } else Console.WriteLine("---Книг нет!");
        }
    }
}
