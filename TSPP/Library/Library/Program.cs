﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library {
    class Program {

        static void ShowReaders(List<Reader> readers) {
            if (readers.Count!=0) {
                Console.WriteLine("---Читатели");
                foreach (Reader reader in readers) {
                    reader.Show();
                }
            }
        }

        static void Main(string[] args) {
            Library library = new Library();
            Librarian librarian = new Librarian();
            Manager manager = new Manager();
            List<Reader> readers = new List<Reader>();

            int n = -1;
            while (n!=0){
                Console.WriteLine("Вы:\n" +
                                  "\t1. Читатель\n" +
                                  "\t2. Прив. читатель\n" +
                                  "\t3. Библиотекарь\n" +
                                  "\t4. Менеджер\n" +
                                  "\t0. Завершить");
                n = int.Parse(Console.ReadLine());
                switch(n){
                    case 1: 
                        int z1 = -1;
                        while (z1 != 0) {
                            Console.WriteLine("\t1. Посмотреть каталог\n" +
                                              "\t2. Взятые книги\n" +
                                              "\t0. Выйти");
                            z1 = int.Parse(Console.ReadLine());
                            switch (z1) {
                                case 0: break;
                                case 1:
                                    readers[0].SeeTheCatalog(library);
                                    break;
                                case 2:
                                    readers[0].GetTakenBooksList(library);
                                    break;
                            }
                        }
                        break;
                    case 2:
                    int z2 = -1;
                    while (z2 != 0) {
                        Console.WriteLine("\t1. Посмотреть каталог\n" +
                                          "\t2. Взятые книги\n" +
                                          "\t3. Взять книгу\n" +
                                          "\t4. Вернуть книгу\n" +
                                          "\t0. Выйти");
                        z2 = int.Parse(Console.ReadLine());
                        switch (z2) {
                            case 0: break;
                            case 1:
                                readers[1].SeeTheCatalog(library);
                                break;
                            case 2:
                                readers[1].GetTakenBooksList(library);
                                break;
                        }
                    }
                    break;
                    case 3:
                    int z3 = -1;
                    while (z3 != 0) {
                        Reader tempReader = new Reader(0);
                        if (readers.Count != 0) {
                            ShowReaders(readers);
                            Console.Write("Для какого читателя:");
                            tempReader = readers[int.Parse(Console.ReadLine()) - 1];
                        } else Console.WriteLine("---Читателей нет!");

                        Console.WriteLine("\t1. Посмотреть весь каталог\n" +
                                          "\t2. Посмотреть книги выданные читателю\n" +
                                          "\t3. Проверить возможность выдачи\n" +
                                          "\t4. Выдать книгу\n" +
                                          "\t5. Веруть книгу\n" +
                                          "\t0. Выйти");

                        z3 = int.Parse(Console.ReadLine());
                        switch (z3) {
                            case 0: break;
                            case 1:
                                librarian.SeeAllCatalog(library);
                                break;
                            case 2:
                            librarian.GetTakenBooksListByReader(library, tempReader);
                                break;
                            case 3:
                                
                                break;
                            case 4: 
                                librarian.SeeTheCatalog(library);
                                Console.Write("Какую книгу выдать: ");
                            librarian.TakeBook(library, int.Parse(Console.ReadLine()), tempReader);
                                break;
                            case 5:
                                librarian.SeeIssuedCatalog(library);
                                Console.Write("Какую книгу вернуть: ");
                            librarian.ReturnBook(library, int.Parse(Console.ReadLine()), tempReader);
                                break;
                        }
                    }
                    break;
                    case 4:
                        int z4 = -1;
                        while (z4!=0){
                            Console.WriteLine("\t1. Посмотреть весь каталог\n" +
                                              "\t2. Добавить книгу\n" +
                                              "\t3. Добавить читателя\n" +
                                              "\t0. Выйти");
                            z4 = int.Parse(Console.ReadLine());
                            switch (z4) {
                                case 0: break;
                                case 1:
                                    manager.SeeAllCatalog(library);
                                    break;
                                case 2:
                                    manager.addBook(library);
                                    break;
                                case 3:
                                    Console.Write("Кого добавить?\n\t1. Читатель\n\t2.Привел. читатель\n");
                                    if(int.Parse(Console.ReadLine())==1) manager.AddReader(readers);
                                    else  manager.AddPrivReader(readers);
                                    ShowReaders(readers);
                                    break;
                            }
                        }
                        break;
                }
            }
            library.SeeAllCatalog();
        }
    }
}